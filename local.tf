locals {
  cluster_name = "${var.app_name}-eks"
  vpc_name     = "${var.app_name}-vpc"

  workers_group_defaults = {
    name                  = "count.index" # Name of the worker group. Literal count.index will never be used but if name is not set, the count.index interpolation will be used.
    root_volume_size      = "100"         # root volume size of workers instances.
    root_volume_type      = "gp2"         # root volume type of workers instances, can be 'standard', 'gp2', or 'io1'
    root_iops             = "0"           # The amount of provisioned IOPS. This must be set with a volume_type of "io1".
    ebs_optimized         = true          # sets whether to use ebs optimization on supported types.
    enable_monitoring     = true          # Enables/disables detailed monitoring.
    public_ip             = false         # Associate a public ip address with a worker
    protect_from_scale_in = false         # Prevent AWS from scaling in, so that cluster-autoscaler is solely responsible.
    platform              = "linux"       # Platform of workers. either "linux" or "windows"
  }
}