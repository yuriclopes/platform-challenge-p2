terraform {
  backend "s3" {
    bucket  = "tf-state-infra-yuri"
    key     = "aws/challenge/terraform.tfstate"
    encrypt = "true"
    region  = "us-east-1"
  }
}