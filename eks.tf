module "eks" {
  source = "git::https://github.com/terraform-aws-modules/terraform-aws-eks.git?ref=v12.0.0"

  cluster_name    = local.cluster_name
  cluster_version = "1.16"
  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.private_subnets

  tags = {
    Project = var.app_name
    Info    = "Managed by Terraform"
  }

  worker_groups                  = var.worker_groups
  workers_group_defaults         = local.workers_group_defaults
  attach_worker_cni_policy       = true
  manage_cluster_iam_resources   = true
  manage_worker_iam_resources    = true
  cluster_endpoint_public_access = var.cluster_endpoint_public_access
  cluster_create_security_group  = true
  cluster_enabled_log_types      = var.cluster_enabled_log_types
}