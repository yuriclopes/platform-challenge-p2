app_name           = "challenge"
vpc_cidr           = "10.0.0.0/16"
private_subnets    = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
public_subnets     = ["10.0.100.0/24", "10.0.101.0/24", "10.0.102.0/24"]
single_nat_gateway = true
# Only for challenge
cluster_endpoint_public_access = true

worker_groups = [
  {
    name                 = "worker-group-1"
    instance_type        = "t3.medium"
    asg_min_size         = 2
    asg_desired_capacity = 2
    asg_max_size         = 4
  }
]