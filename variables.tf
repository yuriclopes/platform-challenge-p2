variable "vpc_cidr" {
  description = "CIDR to use on VPC."
  type        = string
}

variable "private_subnets" {
  description = "List of private subnets to use on VPC."
  type        = list(string)
}

variable "public_subnets" {
  description = "List of public subnets to use on VPC."
  type        = list(string)
}

variable "enable_nat_gateway" {
  description = "Defines whether nat gateway will be create to private subnets."
  type        = bool
  default     = true
}

variable "single_nat_gateway" {
  description = "Defines whether will be used a single nat gateway to all private traffic."
  type        = bool
  default     = false
}

variable "enable_dns_hostnames" {
  description = "Indicates whether instances with public IP addresses get corresponding public DNS hostnames."
  type        = bool
  default     = true
}

variable "enable_dns_support" {
  description = "Indicates whether the DNS resolution is supported."
  type        = bool
  default     = true
}

variable "app_name" {
  description = "Name identifier for the project."
  type        = string
}

variable "worker_groups" {
  description = "A list of maps defining worker group configurations."
  type        = any
}

variable "cluster_endpoint_public_access" {
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled."
  type        = bool
  default     = false
}

variable "cluster_enabled_log_types" {
  description = "A list of the desired control plane logging to enable."
  type        = list(string)
  default     = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
}